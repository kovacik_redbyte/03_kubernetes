CREATE TABLE prices
(
    name       TEXT             NOT NULL,
    symbol     TEXT             NOT NULL,
    price      DOUBLE PRECISION NOT NULL,
    volume_24  DOUBLE PRECISION NOT NULL,
    market_cap DOUBLE PRECISION NOT NULL,
    time       TIMESTAMPTZ      NOT NULL,
    PRIMARY KEY (symbol, time)
);