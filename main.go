package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/lib/pq"
)

const (
	// environment variables
	EnvDbHost     = "DATABASE_HOST"
	EnvDbPort     = "DATABASE_PORT"
	EnvDbUsername = "DATABASE_USERNAME"
	EnvDbPassword = "DATABASE_PASSWORD"
	EnvDbName     = "DATABASE_NAME"
)

func main() {
	// env
	dbHost := os.Getenv(EnvDbHost)
	dbPort := os.Getenv(EnvDbPort)
	dbUsername := os.Getenv(EnvDbUsername)
	dbPassword := os.Getenv(EnvDbPassword)
	dbName := os.Getenv(EnvDbName)

	if dbHost == "" {
		log.Fatalf("%s env variable is required", EnvDbHost)
	}
	if dbPort == "" {
		log.Fatalf("%s env variable is required", EnvDbPort)
	}
	if dbUsername == "" {
		log.Fatalf("%s env variable is required", EnvDbUsername)
	}
	if dbPassword == "" {
		log.Fatalf("%s env variable is required", EnvDbPassword)
	}
	if dbName == "" {
		log.Fatalf("%s env variable is required", EnvDbName)
	}

	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		dbHost, dbPort, dbUsername, dbPassword, dbName)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatalf("error connecting to DB: %v", err)
	}
	defer func(db *sql.DB) {
		_ = db.Close()
	}(db)

	if err = db.Ping(); err != nil {
		log.Fatalf("error connecting to DB: %v", err)
	}

	http.HandleFunc("/price", symbolHandlerFunc(db))
	log.Println("spustam cryptoprices ...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// http://localhost:8080/price?symbol=BTC
func symbolHandlerFunc(db *sql.DB) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		var symbol string
		if params, ok := req.URL.Query()["symbol"]; ok {
			symbol = params[0]
			var name string
			var price float64
			var volume24 float64
			var marketCap float64
			var tm time.Time
			err := db.QueryRow(`SELECT name, price, volume_24, market_cap, time FROM prices WHERE symbol = $1 ORDER BY time DESC LIMIT 1`, symbol).Scan(&name, &price, &volume24, &marketCap, &tm)
			if err != nil && err == sql.ErrNoRows {
				w.WriteHeader(404)
				return
			} else if err != nil {
				w.WriteHeader(500)
				log.Printf("error querying DB: %v", err)
				return
			} else {
				_, err = fmt.Fprintf(w, "symbol=%s\nname=%s\nprice=%f\nvolume24h=%f\nmarket capital=%f\ntime=%v", symbol, name, price, volume24, marketCap, tm)
				if err != nil {
					w.WriteHeader(500)
					log.Printf("error writing response: %v", err)
					return
				}
				return
			}
		}
		w.WriteHeader(404)
	}
}
